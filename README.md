

# Rapport de test de recrutement iFollow

### Nom : ANDRINDRAINY Fiderana
### email : f.andrindrainy@gmail.com
### tel : 0760439944

### 3. Téléopération à distance

Pour cette partie, il sera nécessaire d'installer quelques bibliothèques utilisés par notre programme :

$ sudo pip install getkey
$ sudo pip install paho-mqtt
$ sudo apt install ufw
$ sudo ufw enable
$ sudo apt install mosquitto mosquitto-clients


Ainsi, pour utiliser ce module, il sera demander de lancer le serveur MQTT Broker

$mosquitto -p 1883

Puis, de lancer les deux scripts Pythons dans le fichier source:
$ roscore
$ rosrun mqtt_ros_pkg mqtt_listenner.py
$ rosrun mqtt_ros_pkg script_teleop.py

