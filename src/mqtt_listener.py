import paho.mqtt.client as mqtt #import the client1
import time
import rospy
import std_msgs
import rospkg
from getkey import getkey, keys

from geometry_msgs.msg import Twist
############
def on_message(client, userdata, message):
    print("message received " ,str(message.payload.decode("utf-8")))
    print("message topic=",message.topic)
    print("message qos=",message.qos)
    print("message retain flag=",message.retain)
    if str(message.payload.decode("utf-8")) == "UP" :
        # print("robot must move forward")
        velocity.linear.x=0.2
        velocity.linear.y=0
        velocity.linear.z=0        
        velocity.angular.x=0
        velocity.angular.y=0
        velocity.angular.z=0
    elif str(message.payload.decode("utf-8")) == "DOWN" :
        # print("robot must move backward")
        velocity.linear.x=-0.2
        velocity.linear.y=0
        velocity.linear.z=0        
        velocity.angular.x=0
        velocity.angular.y=0
        velocity.angular.z=0
    elif str(message.payload.decode("utf-8")) == "RIGHT" :
        # print("robot must move Clockwise")
        velocity.linear.x=0
        velocity.linear.y=0
        velocity.linear.z=0        
        velocity.angular.x=0
        velocity.angular.y=0
        velocity.angular.z=-0.5
    elif str(message.payload.decode("utf-8")) == "LEFT" :
        # print("robot must move CounterClockwise")
        velocity.linear.x=0
        velocity.linear.y=0
        velocity.linear.z=0        
        velocity.angular.x=0
        velocity.angular.y=0
        velocity.angular.z=0.5
    elif str(message.payload.decode("utf-8")) == "STOP" :  #stop
        velocity.linear.x=0
        velocity.linear.y=0
        velocity.linear.z=0        
        velocity.angular.x=0
        velocity.angular.y=0
        velocity.angular.z=0
    m_client_pub_ros.publish(velocity)
########################################
broker_address="127.0.0.1"

print("creating new instance")
rospy.init_node("mqtt_listener")
m_client_pub_ros = rospy.Publisher("/cmd_web", Twist, queue_size= 10)
# m_client_pub_ros = rospy.Publisher("/cmd_vel", Twist, queue_size= 10)
velocity = Twist() #variable de consigne de vitesse à publier

client = mqtt.Client("S1") #create new instance
client.on_message=on_message #attach function to callback
print("connecting to broker")
client.connect(broker_address) #connect to broker
client.loop_start() #start the loop



print("Subscribing to topic","topic_teleop")
client.subscribe("topic_teleop")
# print("Publishing message to topic","house/bulbs/bulb1")
# client.publish("house/bulbs/bulb1","OFF")
# time.sleep(4) # wait
# client.publish("house/bulbs/bulb1","ON")
#time.sleep(40) # wait
while(1):
    key = getkey()
    break
client.loop_stop() #stop the loop
