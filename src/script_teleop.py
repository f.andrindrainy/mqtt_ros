import paho.mqtt.client as mqtt #import the client1
import time
from getkey import getkey, keys
def on_message(client, userdata, message):
    print("message received " ,str(message.payload.decode("utf-8")))
    print("message topic=",message.topic)
    
broker_address="127.0.0.1"
print("creating new instance")
client = mqtt.Client("P1") #create new instance

client.connect(broker_address) #connect to broker
client.loop_start() #start the loop

while(1):
    key = getkey()
    if key== keys.UP:
        print ("UP")
        client.publish("topic_teleop","UP")

    if key== keys.DOWN:
        print ("DOWN")
        client.publish("topic_teleop","DOWN")

    if key== keys.LEFT:
        print ("LEFT")
        client.publish("topic_teleop","LEFT")

    if key== keys.RIGHT:
        print ("RIGHT")
        client.publish("topic_teleop","RIGHT")

    elif key== 'a':
        break
    elif key==' ': 
        client.publish("topic_teleop","STOP")    
    # print (direction) 
    #time.sleep(0.1)
client.loop_stop() #stop the loop
